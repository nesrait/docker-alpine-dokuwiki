DokuWiki docker container
=========================


To run image:
-------------

```shell
	docker run -d -p 80:80 --name dokuwiki aranokai/alpine-dokuwiki
```

You can now visit the install page to configure your new DokuWiki wiki.

For example, if you are running container locally, you can acces the page
in browser by going to http://127.0.0.1/install.php

To run with data container:
---------------------------

You can create data container for ease update proccess.

Create data container:

```shell
	docker run --volumes-from dokuwiki --name dokuwiki-data busybox
```

Now you can safely delete dokuwiki container:

```shell
	docker stop dokuwiki && docker rm dokuwiki
```

To restore dokuwiki, create new dokuwiki container and attach dokuwiki-data volume to it:

```shell
	docker run -d -p 80:80 --name dokuwiki --volumes-from dokuwiki-data aranokai/alpine-dokuwiki
```

To backup:
----------

This will create dw-backup.tgz in Your current directory

```shell
	docker run --rm --volumes-from dokuwiki -v $(pwd):/backup aranokai/alpine-dokuwiki tar -czf /backup/dw-backup.tgz /dokuwiki
```

If you created data container, just use `--volumes-from dokuwiki-data`

To restore:
-----------

This will restore content from dw-backup.tgz in Your current directory

```shell
	docker run --rm --volumes-from dokuwiki -v $(pwd):/backup aranokai/alpine-dokuwiki tar -xzf /backup/dw-backup.tgz
```

If you created data container, just use `--volumes-from dokuwiki-data`

There is a possibility of divergence in the permissions of current container and backup. To fix it run:

```shell
	docker run --rm --volumes-from dokuwiki aranokai/alpine-dokuwiki chown -R lighttpd:lighttpd /dokuwiki
```

To update the image:
-------------------

Make backup or use data container (described above)

Update image:

```shell
	docker pull aranokai/alpine-dokuwiki:<desired_tag_here>
```

Restore backup/run with data container

Optimizing your wiki
--------------------

Lighttpd configuration also includes rewrites, so you can enable
nice URLs in settings (Advanced -> Nice URLs, set to ".htaccess")

For better performance enable xsendfile in settings.
Set to proprietary lighttpd header (for `lighttpd < 1.5`)

SystemD service
---------------

[Here is simple systemd service file](https://bitbucket.org/Arano-kai/docker-alpine-dokuwiki/raw/master/docker-dokuwiki.service).
Copy it in `/etc/systemd/system/` and run:

```shell
	systemctl daemon-reload
	systemctl start docker-dokuwiki
```

Build your own
--------------

```shell
	git clone https://bitbucket.org/Arano-kai/docker-alpine-dokuwiki.git
	cd docker-alpine-dokuwiki
	docker build -t my_dokuwiki ./buildroot/
```

Wanna participate?
------------------

Make pull request ;)
But consider several rules please:

- This repo use `git-flow` workflow, so no actual pulls in `master`, do your changes in `develop`
- If change is massive or introduse some intresting feature, use `feature/<cool_name>` branch instead.
- Small critical fixes may go in `hotfix/<version_num>`, branched from `master` (please, contact with maintainer **before** do such pulls)
- Or you can just RTFM of `git-flow` and sort out everything yourself (but who reads those damn manuals? ^_^ )
